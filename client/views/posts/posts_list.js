Template.postsList.helpers({
	posts: function() {
		return Posts.find({}, {sort: {submitted: -1}});
	}
});

/* eg that 'this' can be used in posts_list too
Template.postsList.events({
	'click h3': function () {
		console.log(this.title);
	}
});
*/